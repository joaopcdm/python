from math import sqrt, ceil, floor

num = int(input('Digite um número: '))
raíz = sqrt(num)
print('A raíz quadrada exata de {} vale {:.3f}!'.format(num, raíz))
print('A raíz quadrada arredondada para cima de {} vale {}!'.format(num, ceil(raíz)))
print('A raíz quadrada arredondada para baixo de {} vale {}!'.format(num, floor(raíz)))
