#Tipos primitivos são INT, FLOAT, BOOL e STR

#Detectar o tipo de um valor
#tipoDefault = input('Digite algo: ') - Por padrão é sempre STRING
#tipoInt = int(input('Digite algo: '))
#tipoFloat = float(input('Digite algo: '))
#tipoBool = bool(input('Digite um valor: '))
#tipoStr = str(input('Digite um valor: '))
#print(type(tipo))

####################
     #TEORIA
####################
print('#################### TEORIA ####################')
#Is Numeric? - Verifica se o valor pode ser transformado no tipo primitivo INT
e = input('Digite algo para ver se é numérico: ')
print(e.isnumeric())

#Is Alpha? - Verifica se o valor pode ser transformado no tipo primitivo STR
e = input('Digite algo para ver se é alfabético: ')
print(e.isalpha())

#Is Alphanumeric? - Verifica se o valor possui tipos primitivos STR e INT
e = input('Digite algo para ver se é alfanumérico: ')
print(e.isalnum())

#Entre outros Is'

####################
     #DESAFIOS
####################
print('#################### DESAFIOS ####################')

#Soma entre dois números e mostre seu tipo primitivo
print('1. Soma entre números e detalhes do tipo primitivo do resultado')
n1 = int(input('Digite um número: '))
n2 = int(input('Digite outro número: '))
s = n1+ n2
print('A soma entre {} e {} vale {}! \nO tipo primitivo de {} é {}'.format(n1, n2, s, s, type(s)))
print('')

#Todos os detalhes de um valor
print('2. Todos os detalhes de um valor')
arg = input('Digite algo: ')
print('É numérico? {}'.format(arg.isnumeric()))
print('É alfabético? {}'.format(arg.isalpha()))
print('É alfanumérico? {}'.format(arg.isalnum()))
print('É decimal? {}'.format(arg.isdecimal()))
print('É todo minúsculo? {}'.format(arg.islower()))
print('É todo maiúsculo? {}'.format(arg.isupper()))
print('É composto por espaços? {}'.format(arg.isspace()))
print('É capitalizado? {}'.format(arg.istitle()))
print('')