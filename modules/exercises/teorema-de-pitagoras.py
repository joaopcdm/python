from math import hypot, floor

catetoOposto = float(input('Valor do cateto oposto: '))
catetoAdja = float(input('Valor do cateto adjacente: '))
hipotenusa = hypot(catetoOposto, catetoAdja)
print('O valor da hipotenusa é igual a {}'.format(floor(hipotenusa)))
