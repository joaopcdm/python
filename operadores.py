####################
     #TEORIA
####################
print('#################### TEORIA ####################')

# + Soma | 5 + 2 = 7
# - Subtração | 5 - 2 = 3
# * Multiplicação | 5 * 2 = 10
# / Divisão | 5 / 2 = 2.5
# ** Potência | 5 ** 2 = 25 OU pow(5,2)
# // Divisão inteira | 5 // 2 = 2
# % Resto da divisão 5 % 2 =  1
# Raíz quadrada | 81 ** (1/2)

# Odem de precedência
# 1 -> Parênteses ()
# 2 -> Potências **
# 3 -> Multiplicação *, a divisão /, a divisão inteira // e o resto da divisão %
# 4 -> Soma + e subtração -

print('A teoria se encontra no código...')
print('')

####################
     #DESAFIOS
####################
print('#################### DESAFIOS ####################')

print('1. Detalhes da função print()')
nome = input('Qual o seu nome? ')
print('Prazer em te conhecer, {:-^20}!'.format(nome))
print('')

print('2. Operadores aritmétricos')
n1 = int(input('Digite um valor: '))
n2 = int(input('Digite outro valor: '))
s = n1 + n2
sub = n1 - n2
m = n1 * n2
d = n1 / n2
di = n1 // n2
p = n1 ** n2
print('A soma entre {} e {} vale {:.2f}!'.format(n1, n2, s))
print('A subtração entre {} e {} vale {:.2f}!'.format(n1, n2, sub))
print('A multiplicação entre {} e {} vale {:.2f}!'.format(n1, n2, m))
print('A divisão de {} por {} vale {:.2f}!'.format(n1, n2, d))
print('A divisão inteira entre {} e {} vale {:.2f}!'.format(n1, n2, di))
print('A potência de {} elevado a {} vale {:.2f}!'.format(n1, n2, p))
# ":.2f" significa: Duas casas decimais flutuantes
print('')

print('3. Antecessor e sucessor')
n = int(input('Digite um número: '))
print('Analisando o valor {}, seu antecessor é {} e seu sucessor é {}!'.format(n, (n-1), (n+1)))
print('')

print('4. Dobro, triplo e raíz quadrada')
n = int(input('Digite um número: '))
d = n * 2
t = n * 3
r = n ** (1/2)
print('Seu dobro é {}.'.format(d))
print('Seu triplo é {}.'.format(t))
print('E sua raíz quadrada é {:.2f}!'.format(r))
print('')

print('5. Média de notas')
n1 = float(input('Qual a primeira nota do aluno? '))
n2 = float(input('Qual a segunda nota do aluno? '))
m = (n2 + n1) / 2
print('A média entre {} e {} é igual a {:.1f}!'.format(n1, n2, m))
print('')

print('6. Conversor de medidas')
d = float(input('Uma distância em metros: '))
km = d / 1000
hm = d / 100
dam = d / 10
dm = d * 10
cm = d * 100
mm = d * 1000
print('A medida de {:.1f}m corresponde a:'.format(d))
print('{}km'.format(km))
print('{}hm'.format(hm))
print('{}dam'.format(dam))
print('{}dm'.format(dm))
print('{}cm'.format(cm))
print('{}mm'.format(mm))
print('')

print('7. Tabuada')
n = int(input('Digite um número para ver sua tabuada: '))
print('-' * 12)
print('{} x {:2} = {}'.format(n, 1, (n*1)))
print('{} x {:2} = {}'.format(n, 2, (n*2)))
print('{} x {:2} = {}'.format(n, 3, (n*3)))
print('{} x {:2} = {}'.format(n, 4, (n*4)))
print('{} x {:2} = {}'.format(n, 5, (n*5)))
print('{} x {:2} = {}'.format(n, 6, (n*6)))
print('{} x {:2} = {}'.format(n, 7, (n*7)))
print('{} x {:2} = {}'.format(n, 8, (n*8)))
print('{} x {:2} = {}'.format(n, 9, (n*9)))
print('{} x {:2} = {}'.format(n, 10, (n*10)))
print('-' * 12)
print('')

print('7. Conversor de moedas')
r = float(input('Quantos reais você tem na carteira? R$'))
d = r / 3.72
print('Com R${:.2f} você pode comprar US${:.2f}!'.format(r, d))
print('')

print('8. Pintando parede')
lar = float(input('Largura da parede: '))
alt = float(input('Altura da parede: '))
area = lar * alt
litros = area / 2
print('Sua parede tem a dimensão de {}x{} e sua área é de {:.3f}m².'.format(lar, alt, area))
print('Para pintar essa parede, você precisará de {} litros de tinta.'.format(litros))
print('')

print('9. Cálculo de descontos')
preço = float(input('Qual é o preço do produto? R$'))
desconto = float(input('Qual é a porcentagem de desconto? '))
novopreço = preço - (preço * desconto / 100)
print('O produto que custava R${} passou a custar R${:.2f} com {}% de desconto!'.format(preço, novopreço, desconto))
print('')

print('10. Reajuste salarial')
salário = float(input('Qual o salário do funcionário? R$'))
aumento = float(input('Qual o aumento percentual que será dado ao funcionário? '))
novosalário = salário + (salário * aumento / 100)
print('O funcionário que ganhava R${:.2f} passou a ganhar R${:.2f} com {}% de aumento salarial!'.format(salário, novosalário, aumento))
print('')

print('11. Conversor de temperaturas')
c = float(input('Informe a temperatura em ºC: '))
# Por conta da ordem de precedência, não é necessário parênteses
# f = ((9 * c) / 5) + 32
f = 9 * c / 5 + 32
print('{:.1f}ºC equivale a {:.1f}ºF'.format(c, f))
print('')

print('12. Aluguel de carro')
km = int(input('Quantos quilômetros foram percorridos? '))
dias = int(input('Quantos dias o carro foi alugado? '))
total = dias * 60 + km * 0.15
print('O total a pagar é de R${:.2f}!'.format(total))
print('')
