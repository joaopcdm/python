from math import sin, cos, tan, ceil

a = float(input('Digite um ângulo: '))
sen = sin(a)
cos = cos(a)
tan = tan(a)
print('O seno de {}º vale {}'.format(a, ceil(sen)))
print('O cosseno de {}º vale {}'.format(a, ceil(cos)))
print('A tangente de {}º vale {}'.format(a, ceil(tan)))
